# 已经弃赛

# future TODO
1. 牛视频version 1.0
* 前后端联调，完成基本功能 **后端接口已经玩成，未联调**
* 对系统进行压力测试和性能测试，完成version 1.0的测试报告 **性能测试，功能测试完成**
* 完成Redis中的Hash存储，加快响应速度 **完成一部分demo和测试**
* 完成Redis和Mysql中的数据同步问题===>Redis定时任务 **待完成**
2. version 2.0
* 结合es实现视频搜索的功能
* 使用rabbitmq进行数据的削锋处理
* 结合Java模块的netty达成IM用户通讯
* pprof进行测试，分析性能瓶颈
* 完成version 2.0的分析报告
3. version 3.0
* 项目部署(docker/k8s/shell?)
* 数据库做读写分离
* ETCD做注册中心，拉取服务
* 配置视频流服务的集群
* 最后的压力测试和性能测试，做version 3.0测试报告
4. version 4.0
* 修改前端样式，优化用户体验
5. 记住的问题
* gorm 中插入主键的问题 **结构体内指定 `gorm:"primaryKey"` 字段**
* Hertz中CORS文档的问题 **cloudwego中提供有关跨域文档有错误，具体参考本仓库CORS的配置**
* 未指定字段的信息 
* gorm中遇到的外键问题 **gorm:"-"**