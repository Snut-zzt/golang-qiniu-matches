package db

/*
操作Redis的工具类
*/
func Set(key string, value interface{}) func() error {
	return RS.Set(key, value, 0).Err
}

func Get(key string, value interface{}) func() string {
	return RS.Get(key).Scan(value).Error
}
