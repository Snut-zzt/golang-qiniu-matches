package middleware

import (
	"context"
	"github.com/cloudwego/hertz/pkg/app"
	"github.com/cloudwego/hertz/pkg/protocol/consts"
	"go.uber.org/zap"
	"log"
	"qiniu/cmd/api/db"
)

// 一个进行鉴权的中间件
func LoginMiddleware() app.HandlerFunc {
	return func(ctx context.Context, c *app.RequestContext) {
		token := ""
		zap.S().Infof("进入了鉴权中间件")
		log.Println("==============================================")
		log.Println(c.Query("token"))
		log.Println(c.Query("action_type"))
		log.Println("==============================================")
		if c.Query("token") == "" {
			token = c.PostForm("token")
		} else {
			token = c.Query("token")
		}
		user_id, _ := db.RS.Get(token).Result()
		log.Println(user_id)
		log.Println("上面一行就是值")
		if user_id == "" {
			c.JSON(consts.StatusOK, map[string]interface{}{
				"status_code": 403,
				"status_msg":  "you havent login",
				"data":        nil,
			})
			c.Abort()
		}
		c.Next(ctx)
	}
}
