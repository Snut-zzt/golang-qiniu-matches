package rpc

import (
	"context"
	"github.com/cloudwego/kitex/client"
	"go.uber.org/zap"
	"log"
	"qiniu/cmd/base/kitex_gen/base/example"
	demo "qiniu/cmd/base/kitex_gen/base/example/videoservice"
	"time"
)

var service demo.Client

/*
1.此处应该init一个grpc的全局连接，以供后来调用
*/
func initBaseService() {
	//监听端口
	c, err := demo.NewClient("base-server",
		client.WithHostPorts("localhost:8888"),
		client.WithConnectTimeout(100000*time.Millisecond),
	)
	if err != nil {
		zap.S().Error("未拉取到【base-service】服务")
	}
	service = c
}

// 查询用户详情  从 base 进行 rpc 调用 TODO 绑定 ETCD / Nacos注册中心
func QueryUserDetail(ctx context.Context, req *example.UserDetailReq) *example.UserDetailResp {
	resp, err := service.UserDetail(ctx, req)
	if err != nil {
		log.Println(err.Error())
	}
	funcResp := example.UserDetailResp{
		User:       resp.User,
		StatusMsg:  resp.StatusMsg,
		StatusCode: resp.StatusCode,
	}
	return &funcResp
}

// 用户注册接口
func UserRegister(ctx context.Context, req *example.RegisterReq) *example.RegisterResp {
	zap.S().Info("==============>service服务是否为空<==============")
	log.Println(service)
	resp, err := service.UserRegister(ctx, req)
	funcResp := example.RegisterResp{}
	if err != nil {
		log.Println(err.Error())
	}
	if resp.StatusCode != 200 {
		funcResp = example.RegisterResp{
			Token:      "",
			StatusMsg:  resp.StatusMsg,
			UserId:     0,
			StatusCode: resp.StatusCode,
		}
	}
	funcResp = example.RegisterResp{
		Token:      resp.Token,
		StatusMsg:  resp.StatusMsg,
		UserId:     resp.UserId,
		StatusCode: resp.StatusCode,
	}
	return &funcResp
}
func UserLoginPwd(ctx context.Context, req *example.UserLoginPwdReq) *example.UserLoginPwdResp {
	resp, err := service.UserLoginPwd(ctx, req)
	if err != nil {
		log.Println(err.Error())
	}
	funcResp := example.UserLoginPwdResp{
		Token:      resp.Token,
		StatusMsg:  resp.StatusMsg,
		UserId:     resp.UserId,
		StatusCode: resp.StatusCode,
	}
	return &funcResp
}
func QueryVideoFeed(ctx context.Context, req *example.VideoFeedReq) *example.VideoFeedResp {
	resp, err := service.VideoFeed(ctx, req)
	if err != nil {
		zap.S().Error(err)
	}
	funcResp := example.VideoFeedResp{
		Videos:     resp.Videos,
		StatusMsg:  resp.StatusMsg,
		StatusCode: resp.StatusCode,
	}
	return &funcResp
}
func QueryVideoList(ctx context.Context, req *example.VideoListReq) *example.VideoListResp {
	resp, err := service.QueryPublishList(ctx, req)
	if err != nil {
		zap.S().Error(err)
	}
	funcResp := example.VideoListResp{
		Videos:     resp.Videos,
		StatusMsg:  resp.StatusMsg,
		StatusCode: resp.StatusCode,
	}
	return &funcResp
}
