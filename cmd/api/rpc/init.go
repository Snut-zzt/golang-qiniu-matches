package rpc

import "go.uber.org/zap"

func InitRPC() {
	zap.S().Infof("<===========生成grpc配置 【base-service】====================>")
	zap.S().Infof("<===========生成grpc配置 【socail-service】====================>")
	initBaseService()
	initSocialService()
}
