package rpc

import (
	"context"
	"github.com/cloudwego/kitex/client"
	"go.uber.org/zap"
	example "qiniu/cmd/social/kitex_gen/social/example"
	demo "qiniu/cmd/social/kitex_gen/social/example/socialservice"
	"time"
)

var socialService demo.Client

func initSocialService() {
	//监听端口
	c, err := demo.NewClient("social-server",
		client.WithHostPorts("localhost:8801"),
		client.WithConnectTimeout(100000*time.Millisecond),
	)
	if err != nil {
		zap.S().Error("未拉取到【example-service】服务")
	}
	socialService = c
	if socialService != nil {
		zap.S().Info("【social-service】配置成功")
	} else {
		zap.S().Info("【social-service】配置失败")
	}
}

// 赞操作
func FavoriteActionRPC(ctx context.Context, req *example.FavoriteActionReq) *example.FavoriteActionResp {
	Resp := &example.FavoriteActionResp{}
	Resp, err := socialService.FavoriteAction(ctx, req)
	if err != nil {
		zap.S().Error(err)
		Resp.StatusMsg = err.Error()
		Resp.StatusCode = 500
		return Resp
	}
	Resp.StatusMsg = "success"
	Resp.StatusCode = 200
	return Resp
}

// 查询喜欢视频列表
func FavoriteListRPC(ctx context.Context, req *example.FavoriteListReq) *example.FavoriteListResp {
	Resp := &example.FavoriteListResp{}
	Resp, err := socialService.FavoriteList(ctx, req)
	if err != nil {
		zap.S().Error(err)
		return Resp
	}
	return Resp
}

// 查询评论列表
func CommentActionRPC(ctx context.Context, req *example.CommentActionReq) *example.CommentActionResp {
	Resp := &example.CommentActionResp{}
	Resp, err := socialService.CommentAction(ctx, req)
	if err != nil {
		zap.S().Error(err)
		return Resp
	}
	return Resp
}

// /用户关注操作接口
func QueryCommentListRPC(ctx context.Context, req *example.QueryCommentListReq) *example.QueryCommentListResp {
	Resp := &example.QueryCommentListResp{}
	Resp, err := socialService.QueryCommentList(ctx, req)
	if err != nil {
		zap.S().Error(err)
		return Resp
	}
	return Resp
}

// 查询用户关注列表接口
func LoveActionRPC(ctx context.Context, req *example.LoveActionReq) *example.LoveActionResp {
	Resp := &example.LoveActionResp{}
	Resp, err := socialService.LoveAction(ctx, req)
	if err != nil {
		zap.S().Error(err)
		return Resp
	}
	return Resp
}

// 7查询用户粉丝列表的接口
func QueryLoveListRPC(ctx context.Context, req *example.QueryLoveListReq) *example.QueryLoveListResp {
	Resp := &example.QueryLoveListResp{}
	Resp, err := socialService.QueryLoveList(ctx, req)
	if err != nil {
		zap.S().Error(err)
		return Resp
	}
	return Resp
}

// 8查询用户好友列表的接口
func QueryFansListRPC(ctx context.Context, req *example.QueryFansListReq) *example.QueryFansListResp {
	Resp := &example.QueryFansListResp{}
	Resp, err := socialService.QueryFansList(ctx, req)
	if err != nil {
		zap.S().Error(err)
		return Resp
	}
	return Resp
}

// 查询自己的朋友
func QueryFriendListRPC(ctx context.Context, req *example.QueryFriendListReq) *example.QueryFriendListResp {
	Resp := &example.QueryFriendListResp{}
	Resp, err := socialService.QueryFriendList(ctx, req)
	if err != nil {
		zap.S().Error(err)
		return Resp
	}
	return Resp
}
