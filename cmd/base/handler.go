package main

import (
	"context"
	"go.uber.org/zap"
	example "qiniu/cmd/base/kitex_gen/base/example"
	"qiniu/cmd/base/service"
	"strconv"
)

/*
	rpc远程调用部分
*/
// VideoServiceImpl implements the last service interface defined in the IDL.
type VideoServiceImpl struct{}

// VideoFeed implements the VideoServiceImpl interface.
func (s *VideoServiceImpl) VideoFeed(ctx context.Context, req *example.VideoFeedReq) (resp *example.VideoFeedResp, err error) {
	// TODO: Your code here...
	zap.S().Infof("进入视频留======================")
	videoList, _, msg := service.QueryVideoFeed(ctx, req.LatestTime, req.Token)
	zap.S().Infof("查询完毕======================")
	Resp := example.VideoFeedResp{
		Videos:     videoList,
		StatusMsg:  msg,
		NextTime:   "",
		StatusCode: 200,
	}
	return &Resp, nil
}

// QueryPublishList implements the VideoServiceImpl interface.
// 查询用户的发布列表 ,发布列表的逻辑和上面的逻辑一样
func (s *VideoServiceImpl) QueryPublishList(ctx context.Context, req *example.VideoListReq) (resp *example.VideoListResp, err error) {
	// TODO: Your code here... 这个是直接给user_id来进行查询的
	zap.S().Infof("进入视频留======================")
	IntUserId, _ := strconv.Atoi(req.UserId)
	videoList, _, msg := service.QueryVideoList(ctx, "", req.Token, int64(IntUserId))
	zap.S().Infof("查询完毕======================")
	Resp := example.VideoListResp{
		Videos:     videoList,
		StatusMsg:  msg,
		StatusCode: 200,
	}
	return &Resp, nil
}

// UserDetail implements the VideoServiceImpl interface.
func (s *VideoServiceImpl) UserDetail(ctx context.Context, req *example.UserDetailReq) (resp *example.UserDetailResp, err error) {
	// TODO: Your code here...
	num, _ := strconv.Atoi(req.UserId)
	user, _ := service.QueryUserById(ctx, int64(num))
	Resp := example.UserDetailResp{
		User:       user,
		StatusMsg:  "success",
		StatusCode: 200,
	}
	return &Resp, nil
}

// UserRegister implements the VideoServiceImpl interface.
func (s *VideoServiceImpl) UserRegister(ctx context.Context, req *example.RegisterReq) (resp *example.RegisterResp, err error) {
	// TODO: Your code here...
	Resp := example.RegisterResp{}
	token, err, user_id, code := service.UserRegister(ctx, req.Username, req.Password)
	if err != nil {
		Resp = example.RegisterResp{
			Token:      token,
			StatusMsg:  "fail to register",
			UserId:     user_id,
			StatusCode: 500,
		}
		return &Resp, err
	}
	Resp = example.RegisterResp{
		Token:      token,
		StatusMsg:  "success",
		UserId:     user_id,
		StatusCode: 200,
	}
	switch code {
	case 10001:
		Resp.StatusCode = 10001
		Resp.StatusMsg = "Duplicate username"
	case 200:
		Resp.StatusCode = 200
		Resp.StatusMsg = "success to register"
	case 500:
		Resp.StatusCode = 500
		Resp.StatusMsg = "server has something wrong"
	}

	return &Resp, nil
}

// UserLoginCode implements the VideoServiceImpl interface.
func (s *VideoServiceImpl) UserLoginCode(ctx context.Context, req *example.UserLoginCodeReq) (resp *example.UserLoginCodeResp, err error) {
	// TODO: Your code here...

	return
}

// UserLoginPwd implements the VideoServiceImpl interface.
func (s *VideoServiceImpl) UserLoginPwd(ctx context.Context, req *example.UserLoginPwdReq) (resp *example.UserLoginPwdResp, err error) {
	// TODO: Your code here...
	Resp := example.UserLoginPwdResp{}
	zap.S().Infof("=====================》进入到了用户注册的密码阶段<======================")
	err, code, token, user_id := service.UserLoginPwd(ctx, req.Username, req.Password)
	Resp.UserId = user_id
	Resp.Token = token
	switch code {
	case 10002:
		Resp.StatusCode = 10002
		Resp.StatusMsg = "you havent register"
	case 200:
		Resp.StatusCode = 200
		Resp.StatusMsg = "success to login"
	case 10003:
		Resp.StatusCode = 10003
		Resp.StatusMsg = "username or password has wrong"
	}
	return &Resp, nil
}
