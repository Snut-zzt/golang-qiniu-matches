namespace go base.example

// 定义视频流接口文档
struct Video {
    1:i64 video_id;
    2:User author;
    3:string play_url;
    4:string cover_url;
    5:string title;
    6:i64 favorite_count;
    7:i64 comment_count;
    8:bool is_favorite;
}

struct User {
    1:i64 user_id;
    2:string  user_name;
    3:i64 follow_count;
    4:i64 follower_count;
    5:bool is_follow;
    6:string avatar;
    7:string background_image;
    8:string  signature;
    9:i64 total_favorited;
    10:i64 work_count;
    11:i64 favorite_count;
}
struct VideoFeedReq{
        1:string latest_time;
        2:string  token;
}
struct VideoFeedResp{
    1:list<Video> videos;
    2:string status_msg;
    3:string next_time;
    4:i64 status_code;
}
// 查询发布列表接口
struct VideoListReq{
        1:string user_id;
        2:string  token;
}
struct VideoListResp{
    1:list<Video> videos;
    2:string status_msg;
    3:i64 status_code;
}
//查询用户详情
struct UserDetailReq{
        1:string user_id;
        2:string  token;
}
struct UserDetailResp{
    1:User user;
    2:string status_msg;
    3:i64 status_code;
}

//用户模块接口,用户注册
struct RegisterReq{
        1:string username;
        2:string  password;
        3:string phone;
}
struct RegisterResp{
    1:string token;
    2:string status_msg;
    3:i64 user_id;
    4:i64 status_code;
}
//用户登录验证码
struct UserLoginCodeReq{
        2:string  password;
        3:string phone;
}
struct UserLoginCodeResp{
    1:string token;
    2:string status_msg;
    3:i64 user_id;
    4:i64 status_code;
}
//用户登录密码
struct UserLoginPwdReq{
        1:string username;
        2:string  password;
}
struct UserLoginPwdResp{
    1:string token;
    2:string status_msg;
    3:i64 user_id;
    4:i64 status_code;
}

service VideoService {
    VideoFeedResp VideoFeed(1:VideoFeedReq req); //视频流接口
    VideoListResp QueryPublishList(1:VideoListReq req);  //查询发布列表
    UserDetailResp UserDetail(1:UserDetailReq req);  //查询用户详情
    RegisterResp UserRegister(1:RegisterReq req); //用户注册
    UserLoginCodeResp UserLoginCode(1:UserLoginCodeReq req); //用户登录code
    UserLoginPwdResp UserLoginPwd(1:UserLoginPwdReq req);  //用户登录code
}