package main

import (
	"github.com/cloudwego/kitex/server"
	"go.uber.org/zap"
	"log"
	"qiniu/cmd/base/db"
	example "qiniu/cmd/base/kitex_gen/base/example/videoservice"
	"time"
)

/*
端口号8888
*/
func main() {
	svr := example.NewServer(new(VideoServiceImpl),
		server.WithReadWriteTimeout(365*24*60*60*time.Second),
		server.WithExitWaitTime(365*24*60*60*time.Second),
	)

	logger, _ := zap.NewDevelopment()
	zap.ReplaceGlobals(logger)
	db.Init()
	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}
