package service

import (
	"context"
	"crypto/md5"
	"crypto/sha512"
	"encoding/hex"
	"fmt"
	"github.com/anaskhan96/go-password-encoder"
	"go.uber.org/zap"
	"io"
	"log"
	"qiniu/cmd/base/db"
	"qiniu/cmd/base/kitex_gen/base/example"
	"qiniu/cmd/base/utils"
	"strings"
)

type User struct {
	UserId   int64
	UserName string
	Password string
}
type Attention struct {
	UserID   int64
	ToUserId int64
}

// 定义加密方法
func genMd5(code string) string {
	Md5 := md5.New()
	_, _ = io.WriteString(Md5, code)
	return hex.EncodeToString(Md5.Sum(nil))
}

// 加密的函数
func EncodingPassword(oldpassword string) string {

	options := &password.Options{16, 100, 32, sha512.New}
	salt, encodedPwd := password.Encode(oldpassword, options)
	newpassword := fmt.Sprintf("$pbkdf2-sha512$%s$%s", salt, encodedPwd)
	return newpassword
}

// 定义解密方法
func DecryptPassword(oldpassword string) bool {
	options := &password.Options{16, 100, 32, sha512.New}
	passwordInfo := strings.Split(oldpassword, "$")
	log.Println(passwordInfo)
	check := password.Verify(oldpassword, passwordInfo[2], passwordInfo[3], options)
	return check
}

// 查询用户的详细信息
func QueryUserById(ctx context.Context, userID int64) (*example.User, error) {
	//在次之前添加一个中间件鉴权
	res := example.User{}
	if err := db.DB.WithContext(ctx).Table("user").Where("user_id = ?", userID).Find(&res).Error; err != nil {
		zap.Error(err)
		return nil, err
	}

	log.Println(res)
	return &res, nil
}

// 用户用密码登录的信息
func UserLoginPwd(ctx context.Context, username string, userpassword string) (error, int64, string, int64) {
	//在次之前添加一个中间件鉴权
	zap.S().Infof("进入到了Dao层")
	res := User{}
	result := db.DB.WithContext(ctx).Table("user").Where("user_name = ?", username).Find(&res)
	zap.S().Infof("用户的数据是")
	log.Println(res)
	zap.S().Infof("影响的行数是")
	log.Println(result.RowsAffected)
	//数据库用户存在进行登录
	if result.RowsAffected != 0 {
		options := &password.Options{16, 100, 32, sha512.New}
		passwordInfo := strings.Split(res.Password, "$")
		log.Println(passwordInfo)
		check := password.Verify(userpassword, passwordInfo[2], passwordInfo[3], options)
		if check {
			zap.S().Infof("密码校验通过")
			token, _ := utils.GenerateToken(username, res.Password)
			zap.S().Infof("准备存入redis")
			db.Set(token, res.UserId)
			zap.S().Infof("生成成功，且成功存入redis中")
			log.Println(res)
			return nil, 200, token, res.UserId //success
		}
	} else {
		zap.S().Infof("用户尚未注册")
		return nil, 10002, "", 0 //还未注册
	}
	zap.S().Infof("用户名或者密码不正确")
	log.Println(res)
	return nil, 10003, "", 0 //用户名或者密码不正确
}

// 注册用户的方法,需要进行密码的加密。
func UserRegister(ctx context.Context, username string, password string) (string, error, int64, int64) {
	//判断用户名是否唯一
	res := example.User{}
	//判断用户是否存在
	result := db.DB.Table("user").Where("user_name = ?", username).First(&example.User{})
	if result.RowsAffected != 0 {
		return "", nil, 0, 10001
	}
	log.Println(res)
	//密码进行加密
	newPwd := EncodingPassword(password)
	//插入数据
	user := User{
		UserName: username,
		Password: newPwd,
	}
	result = db.DB.Table("user").Create(&user) // 通过数据的指针来创建
	token := ""
	if result.RowsAffected == 0 {
		zap.S().Error("insert info wrong")
		return "", nil, 0, 500
	} else {

		db.DB.Table("user").Where("user_name = ?", username).First(&user)
		//生成一个token
		token, _ = utils.GenerateToken(username, newPwd)
		//存入redis中判断token:user_id
		db.Set(token, user.UserId)
	}

	return token, nil, user.UserId, 200
}

// 返回一个video的切片
func QueryVideoFeed(ctx context.Context, time string, token string) ([]*example.Video, error, string) {
	//时间的判断，如果没有时间，那就倒序查数据，如果有时间，那就根据时间查数据
	videoList := make([]*example.Video, 3)
	if time == "" {
		//倒叙
		result := db.DB.Table("videos").
			Order("created_time desc").
			Limit(3).Find(&videoList)
		log.Println(videoList)
		if result.RowsAffected > 0 {
			//把查询的user放入videolist中
			for _, video := range videoList {
				var user example.User
				result = db.DB.
					Table("user").
					Where(video.VideoId).
					Find(&user)
				video.Author = &user
			}
			//遍历videolist查询attention表
			Intuid, _ := db.RS.Get(token).Result()
			log.Println(Intuid)
			for i, video := range videoList {
				var attentions []Attention
				var Isfollow bool
				result := db.DB.Table("attention").Where("user_id = ? AND to_user_id = ?", Intuid, video.Author.UserId).Find(&attentions)
				if result.RowsAffected != 0 {
					Isfollow = true
				}
				videoList[i].IsFavorite = Isfollow
			}
			//封装完毕成功返回
			return videoList, nil, "success" //success
		} else {
			return nil, result.Error, "no video has exist"
		}
	} else {
		//desc,传上来毫秒级别的时间戳
		result := db.DB.Table("videos").
			Where("created_time < ?", time).
			Order("created_time DESC").
			Limit(3).
			Find(&videoList)
		if result.RowsAffected > 0 {
			//把查询的user放入videolist中
			for _, video := range videoList {
				var user example.User
				result = db.DB.
					Table("user").
					Where(video.VideoId).
					Find(&user)
				video.Author = &user
			}
			//遍历videolist查询attention表
			Intuid := db.RS.Get(token)
			for i, video := range videoList {
				var attentions []Attention
				var Isfollow bool
				result := db.DB.Table("attention").Where("user_id = ? AND to_user_id = ?", Intuid, video.Author.UserId).Find(&attentions)
				if result.RowsAffected != 0 {
					Isfollow = true
				}
				videoList[i].IsFavorite = Isfollow
			}
			return videoList, nil, "success" //success
		} else {
			return nil, result.Error, "no video has exist"
		}
	}
}

func QueryVideoList(ctx context.Context, time string, token string, to_user_id int64) ([]*example.Video, error, string) {
	//时间的判断，如果没有时间，那就倒序查数据，如果有时间，那就根据时间查数据 TODO 此处声明切片不是3
	var videoList []*example.Video
	//倒叙
	zap.S().Infof("进入了查询videoList方法")
	log.Println(to_user_id)
	result := db.DB.Table("videos").
		Where("user_id = ?", to_user_id).
		Find(&videoList)
	log.Println(videoList)
	if result.RowsAffected > 0 {
		//把查询的user放入videolist中
		for _, video := range videoList {
			var user example.User
			result = db.DB.
				Table("user").
				Where("user_id = ?", to_user_id).
				Find(&user)
			video.Author = &user
		}
		//遍历videolist查询attention表
		Intuid, _ := db.RS.Get(token).Result()
		log.Println(Intuid)
		for i, _ := range videoList {
			var attentions []Attention
			var Isfollow bool
			result := db.DB.Table("attention").Where("user_id = ? AND to_user_id = ?", Intuid, to_user_id).Find(&attentions)
			if result.RowsAffected != 0 {
				Isfollow = true
			}
			videoList[i].IsFavorite = Isfollow
		}
		//封装完毕成功返回
		return videoList, nil, "success" //success
	} else {
		return nil, result.Error, "no video has exist"
	}

}
