package utils

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)

func GenerateToken(username, password string) (string, error) {
	// 创建一个新的令牌对象
	token := jwt.New(jwt.SigningMethodHS256)

	// 设置声明（Claims）
	claims := token.Claims.(jwt.MapClaims)
	claims["username"] = username
	claims["password"] = password
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix() // 设置令牌过期时间为 24 小时

	// 使用密钥对令牌进行签名
	secretKey := []byte("cloudwego") // 替换为你自己的密钥
	signedToken, err := token.SignedString(secretKey)
	if err != nil {
		return "", err
	}

	return signedToken, nil
}
