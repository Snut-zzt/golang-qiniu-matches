package db

import (
	"github.com/go-redis/redis"
	"go.uber.org/zap"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
)

var (
	DB *gorm.DB
	RS *redis.Client
)

// Init init DB
func Init() {
	zap.S().Info("=============>初始化MYSQL数据库连接<==============")
	var err error
	DB, err = gorm.Open(mysql.Open("root:0927@tcp(43.143.80.216:3306)/douyin?charset=utf8mb4&parseTime=True&loc=Local"),
		&gorm.Config{
			PrepareStmt:            true,
			SkipDefaultTransaction: true,
		},
	)
	if err != nil {
		panic(err)
		zap.S().Error("=============>数据库连接失败<==============")
	}
	/*
		redis的全局连接
	*/
	RS = redis.NewClient(&redis.Options{
		Addr:     "43.143.44.118:9898", // Redis 服务器地址和端口
		Password: "192047",             // Redis 服务器密码，如果没有设置密码则为空字符串
		DB:       0,                    // Redis 数据库索引，默认为 0
	})
	zap.S().Error("=============>Redis初始值<==============")
	log.Println(RS)
}
