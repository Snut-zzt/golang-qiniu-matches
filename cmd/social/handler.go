package main

import (
	"context"
	"qiniu/cmd/social/kitex_gen/social/example"
	"qiniu/cmd/social/service"
)

// SocialServiceImpl implements the last service interface defined in the IDL.
type SocialServiceImpl struct{}

// FavoriteAction implements the SocialServiceImpl interface.
func (s *SocialServiceImpl) FavoriteAction(ctx context.Context, req *example.FavoriteActionReq) (resp *example.FavoriteActionResp, err error) {
	// TODO: Your code here...
	Resp := example.FavoriteActionResp{}
	Resp, err = service.FavoriteActionFunc(ctx, req)
	return &Resp, err
}

// FavoriteList implements the SocialServiceImpl interface.
func (s *SocialServiceImpl) FavoriteList(ctx context.Context, req *example.FavoriteListReq) (resp *example.FavoriteListResp, err error) {
	// TODO: Your code here...
	Resp := example.FavoriteListResp{}
	Resp, err = service.FavoriteListFunc(ctx, req)
	return &Resp, err
}

// CommentAction implements the SocialServiceImpl interface.
func (s *SocialServiceImpl) CommentAction(ctx context.Context, req *example.CommentActionReq) (resp *example.CommentActionResp, err error) {
	// TODO: Your code here...
	Resp := example.CommentActionResp{}
	Resp, err = service.CommentActionFunc(ctx, req)
	return &Resp, err
}

// QueryCommentList implements the SocialServiceImpl interface.
func (s *SocialServiceImpl) QueryCommentList(ctx context.Context, req *example.QueryCommentListReq) (resp *example.QueryCommentListResp, err error) {
	// TODO: Your code here...
	Resp := example.QueryCommentListResp{}
	Resp, err = service.QueryCommentListFunc(ctx, req)
	return &Resp, err
}

// LoveAction implements the SocialServiceImpl interface.
func (s *SocialServiceImpl) LoveAction(ctx context.Context, req *example.LoveActionReq) (resp *example.LoveActionResp, err error) {
	// TODO: Your code here...
	Resp := example.LoveActionResp{}
	Resp, err = service.LoveActionFunc(ctx, req)
	return &Resp, err
}

// QueryLoveList implements the SocialServiceImpl interface.
func (s *SocialServiceImpl) QueryLoveList(ctx context.Context, req *example.QueryLoveListReq) (resp *example.QueryLoveListResp, err error) {
	// TODO: Your code here...
	Resp := example.QueryLoveListResp{}
	Resp, err = service.QueryLoveListFunc(ctx, req)
	return &Resp, err
}

// QueryFansList implements the SocialServiceImpl interface.
func (s *SocialServiceImpl) QueryFansList(ctx context.Context, req *example.QueryFansListReq) (resp *example.QueryFansListResp, err error) {
	// TODO: Your code here...
	Resp := example.QueryFansListResp{}
	Resp, err = service.QueryFansListFunc(ctx, req)
	return &Resp, err
}

// QueryFriendList implements the SocialServiceImpl interface.
func (s *SocialServiceImpl) QueryFriendList(ctx context.Context, req *example.QueryFriendListReq) (resp *example.QueryFriendListResp, err error) {
	// TODO: Your code here...
	Resp := example.QueryFriendListResp{}
	Resp, err = service.QueryFriendListFunc(ctx, req)
	return &Resp, err
}
