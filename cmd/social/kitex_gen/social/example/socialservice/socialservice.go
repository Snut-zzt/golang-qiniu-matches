// Code generated by Kitex v0.7.3. DO NOT EDIT.

package socialservice

import (
	"context"
	client "github.com/cloudwego/kitex/client"
	kitex "github.com/cloudwego/kitex/pkg/serviceinfo"
	example "qiniu/cmd/social/kitex_gen/social/example"
)

func serviceInfo() *kitex.ServiceInfo {
	return socialServiceServiceInfo
}

var socialServiceServiceInfo = NewServiceInfo()

func NewServiceInfo() *kitex.ServiceInfo {
	serviceName := "SocialService"
	handlerType := (*example.SocialService)(nil)
	methods := map[string]kitex.MethodInfo{
		"FavoriteAction":   kitex.NewMethodInfo(favoriteActionHandler, newSocialServiceFavoriteActionArgs, newSocialServiceFavoriteActionResult, false),
		"FavoriteList":     kitex.NewMethodInfo(favoriteListHandler, newSocialServiceFavoriteListArgs, newSocialServiceFavoriteListResult, false),
		"CommentAction":    kitex.NewMethodInfo(commentActionHandler, newSocialServiceCommentActionArgs, newSocialServiceCommentActionResult, false),
		"QueryCommentList": kitex.NewMethodInfo(queryCommentListHandler, newSocialServiceQueryCommentListArgs, newSocialServiceQueryCommentListResult, false),
		"LoveAction":       kitex.NewMethodInfo(loveActionHandler, newSocialServiceLoveActionArgs, newSocialServiceLoveActionResult, false),
		"QueryLoveList":    kitex.NewMethodInfo(queryLoveListHandler, newSocialServiceQueryLoveListArgs, newSocialServiceQueryLoveListResult, false),
		"QueryFansList":    kitex.NewMethodInfo(queryFansListHandler, newSocialServiceQueryFansListArgs, newSocialServiceQueryFansListResult, false),
		"QueryFriendList":  kitex.NewMethodInfo(queryFriendListHandler, newSocialServiceQueryFriendListArgs, newSocialServiceQueryFriendListResult, false),
	}
	extra := map[string]interface{}{
		"PackageName":     "example",
		"ServiceFilePath": `social.thrift`,
	}
	svcInfo := &kitex.ServiceInfo{
		ServiceName:     serviceName,
		HandlerType:     handlerType,
		Methods:         methods,
		PayloadCodec:    kitex.Thrift,
		KiteXGenVersion: "v0.7.3",
		Extra:           extra,
	}
	return svcInfo
}

func favoriteActionHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*example.SocialServiceFavoriteActionArgs)
	realResult := result.(*example.SocialServiceFavoriteActionResult)
	success, err := handler.(example.SocialService).FavoriteAction(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newSocialServiceFavoriteActionArgs() interface{} {
	return example.NewSocialServiceFavoriteActionArgs()
}

func newSocialServiceFavoriteActionResult() interface{} {
	return example.NewSocialServiceFavoriteActionResult()
}

func favoriteListHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*example.SocialServiceFavoriteListArgs)
	realResult := result.(*example.SocialServiceFavoriteListResult)
	success, err := handler.(example.SocialService).FavoriteList(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newSocialServiceFavoriteListArgs() interface{} {
	return example.NewSocialServiceFavoriteListArgs()
}

func newSocialServiceFavoriteListResult() interface{} {
	return example.NewSocialServiceFavoriteListResult()
}

func commentActionHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*example.SocialServiceCommentActionArgs)
	realResult := result.(*example.SocialServiceCommentActionResult)
	success, err := handler.(example.SocialService).CommentAction(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newSocialServiceCommentActionArgs() interface{} {
	return example.NewSocialServiceCommentActionArgs()
}

func newSocialServiceCommentActionResult() interface{} {
	return example.NewSocialServiceCommentActionResult()
}

func queryCommentListHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*example.SocialServiceQueryCommentListArgs)
	realResult := result.(*example.SocialServiceQueryCommentListResult)
	success, err := handler.(example.SocialService).QueryCommentList(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newSocialServiceQueryCommentListArgs() interface{} {
	return example.NewSocialServiceQueryCommentListArgs()
}

func newSocialServiceQueryCommentListResult() interface{} {
	return example.NewSocialServiceQueryCommentListResult()
}

func loveActionHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*example.SocialServiceLoveActionArgs)
	realResult := result.(*example.SocialServiceLoveActionResult)
	success, err := handler.(example.SocialService).LoveAction(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newSocialServiceLoveActionArgs() interface{} {
	return example.NewSocialServiceLoveActionArgs()
}

func newSocialServiceLoveActionResult() interface{} {
	return example.NewSocialServiceLoveActionResult()
}

func queryLoveListHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*example.SocialServiceQueryLoveListArgs)
	realResult := result.(*example.SocialServiceQueryLoveListResult)
	success, err := handler.(example.SocialService).QueryLoveList(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newSocialServiceQueryLoveListArgs() interface{} {
	return example.NewSocialServiceQueryLoveListArgs()
}

func newSocialServiceQueryLoveListResult() interface{} {
	return example.NewSocialServiceQueryLoveListResult()
}

func queryFansListHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*example.SocialServiceQueryFansListArgs)
	realResult := result.(*example.SocialServiceQueryFansListResult)
	success, err := handler.(example.SocialService).QueryFansList(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newSocialServiceQueryFansListArgs() interface{} {
	return example.NewSocialServiceQueryFansListArgs()
}

func newSocialServiceQueryFansListResult() interface{} {
	return example.NewSocialServiceQueryFansListResult()
}

func queryFriendListHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*example.SocialServiceQueryFriendListArgs)
	realResult := result.(*example.SocialServiceQueryFriendListResult)
	success, err := handler.(example.SocialService).QueryFriendList(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newSocialServiceQueryFriendListArgs() interface{} {
	return example.NewSocialServiceQueryFriendListArgs()
}

func newSocialServiceQueryFriendListResult() interface{} {
	return example.NewSocialServiceQueryFriendListResult()
}

type kClient struct {
	c client.Client
}

func newServiceClient(c client.Client) *kClient {
	return &kClient{
		c: c,
	}
}

func (p *kClient) FavoriteAction(ctx context.Context, req *example.FavoriteActionReq) (r *example.FavoriteActionResp, err error) {
	var _args example.SocialServiceFavoriteActionArgs
	_args.Req = req
	var _result example.SocialServiceFavoriteActionResult
	if err = p.c.Call(ctx, "FavoriteAction", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) FavoriteList(ctx context.Context, req *example.FavoriteListReq) (r *example.FavoriteListResp, err error) {
	var _args example.SocialServiceFavoriteListArgs
	_args.Req = req
	var _result example.SocialServiceFavoriteListResult
	if err = p.c.Call(ctx, "FavoriteList", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) CommentAction(ctx context.Context, req *example.CommentActionReq) (r *example.CommentActionResp, err error) {
	var _args example.SocialServiceCommentActionArgs
	_args.Req = req
	var _result example.SocialServiceCommentActionResult
	if err = p.c.Call(ctx, "CommentAction", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) QueryCommentList(ctx context.Context, req *example.QueryCommentListReq) (r *example.QueryCommentListResp, err error) {
	var _args example.SocialServiceQueryCommentListArgs
	_args.Req = req
	var _result example.SocialServiceQueryCommentListResult
	if err = p.c.Call(ctx, "QueryCommentList", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) LoveAction(ctx context.Context, req *example.LoveActionReq) (r *example.LoveActionResp, err error) {
	var _args example.SocialServiceLoveActionArgs
	_args.Req = req
	var _result example.SocialServiceLoveActionResult
	if err = p.c.Call(ctx, "LoveAction", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) QueryLoveList(ctx context.Context, req *example.QueryLoveListReq) (r *example.QueryLoveListResp, err error) {
	var _args example.SocialServiceQueryLoveListArgs
	_args.Req = req
	var _result example.SocialServiceQueryLoveListResult
	if err = p.c.Call(ctx, "QueryLoveList", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) QueryFansList(ctx context.Context, req *example.QueryFansListReq) (r *example.QueryFansListResp, err error) {
	var _args example.SocialServiceQueryFansListArgs
	_args.Req = req
	var _result example.SocialServiceQueryFansListResult
	if err = p.c.Call(ctx, "QueryFansList", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) QueryFriendList(ctx context.Context, req *example.QueryFriendListReq) (r *example.QueryFriendListResp, err error) {
	var _args example.SocialServiceQueryFriendListArgs
	_args.Req = req
	var _result example.SocialServiceQueryFriendListResult
	if err = p.c.Call(ctx, "QueryFriendList", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}
