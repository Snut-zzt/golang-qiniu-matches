package main

import (
	"github.com/cloudwego/kitex/server"
	"go.uber.org/zap"
	"log"
	"net"
	"qiniu/cmd/social/db"
	example "qiniu/cmd/social/kitex_gen/social/example/socialservice"
	"time"
)

func main() {
	addr, _ := net.ResolveTCPAddr("tcp", ":8801")

	var opts []server.Option
	//配置超时控制
	opts = append(opts, server.WithServiceAddr(addr))
	opts = append(opts, server.WithReadWriteTimeout(365*24*60*60*time.Second))
	opts = append(opts, server.WithExitWaitTime(365*24*60*60*time.Second))
	svr := example.NewServer(new(SocialServiceImpl), opts...)

	db.Init()
	logger, _ := zap.NewDevelopment()
	zap.ReplaceGlobals(logger)
	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}
