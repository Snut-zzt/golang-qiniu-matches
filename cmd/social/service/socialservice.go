package service

import (
	"context"
	"go.uber.org/zap"
	"log"
	"qiniu/cmd/social/db"
	"qiniu/cmd/social/kitex_gen/social/example"
	"strconv"
	"time"
)

type Favorites struct {
	UserId     int32
	VideoId    int32
	CreateTime string
}
type Attention struct {
	UserID   int64
	ToUserId int64
}
type Comment struct {
	Content    string
	CreateDate string
	CommentId  int32 `gorm:"primaryKey"`
	VideoId    int32
	UserId     int32
	DeletedAt  string
}

func newTime() string {
	currentTime := time.Now()
	formattedTime := currentTime.Format("2006-01-02 15:04:05")
	return formattedTime
}
func getRedisUserId(token string) int {
	zap.S().Info("传递进来的token")
	log.Println(token)
	Idstring, err := db.RS.Get(token).Result()
	if err != nil {
		zap.S().Info("d1111111111111111111111111")
		log.Println(err.Error())
	}
	user_id, err := strconv.Atoi(Idstring)
	if err != nil {
		zap.S().Info("2222222222222222222222")
		log.Println(err.Error())
	}
	log.Println("传递进来的user_id")
	log.Println(user_id)
	return user_id
}

// 1点赞操作
func FavoriteActionFunc(ctx context.Context, req *example.FavoriteActionReq) (example.FavoriteActionResp, error) {
	Resp := example.FavoriteActionResp{}
	//判断是点赞还是取消点赞
	formattedTime := newTime()
	Id, _ := db.RS.Get(req.Token).Result()
	user_id, _ := strconv.Atoi(Id)
	log.Println("======================看一下id压=========================")
	log.Println(user_id)
	Count := Favorites{
		UserId:     int32(user_id),
		VideoId:    int32(req.VideoId),
		CreateTime: formattedTime,
	}
	if req.ActionType == 1 {
		//点赞，插入数据
		result := db.DB.Table("favorites").Create(&Count)
		if result.RowsAffected == 0 {
			zap.S().Error(result.Error)
			Resp.StatusMsg = "somethings error"
			Resp.StatusCode = 500
			return Resp, result.Error
		} else {
			Resp.StatusMsg = "success"
			Resp.StatusCode = 200
			return Resp, nil
		}
	} else {
		//取消点赞，删除数据
		result := db.DB.Table("favorites").Where("user_id = ? and video_id = ?", user_id, req.VideoId).Delete(&Count)
		if result.RowsAffected == 0 {
			zap.S().Error(result.Error)
			Resp.StatusMsg = "somethings error"
			Resp.StatusCode = 500
			return Resp, result.Error
		} else {
			Resp.StatusMsg = "success"
			Resp.StatusCode = 200
			return Resp, nil
		}
	}
}

// 查询某个作者喜欢视频列表
func FavoriteListFunc(ctx context.Context, req *example.FavoriteListReq) (example.FavoriteListResp, error) {
	Resp := example.FavoriteListResp{}
	var videoList []*example.Video
	//倒叙
	zap.S().Infof("进入了查询FavoriteListFunc方法")
	log.Println(req.UserId)
	//result := db.DB.Table("videos").
	//	Where("user_id = ?", req.UserId).
	//	Find(&videoList)
	subQuery := db.DB.Table("favorites").Select("video_id").Where("user_id = ?", req.UserId)
	result := db.DB.Table("videos").Where("video_id in (?)", subQuery).Find(&videoList)
	log.Println(videoList)
	if result.RowsAffected > 0 {
		//把查询的user放入videolist中
		for _, video := range videoList {
			var user example.User
			result = db.DB.
				Table("user").
				Where("user_id = ?", req.UserId).
				Find(&user)
			video.Author = &user
		}
		//遍历videolist查询attention表
		for i, video := range videoList {
			var attentions []Attention
			var Isfollow bool
			result := db.DB.Table("attention").Where("user_id = ? AND to_user_id = ?", req.UserId, video.Author.UserId).Find(&attentions)
			if result.RowsAffected != 0 {
				Isfollow = true
			}
			videoList[i].IsFavorite = Isfollow
		}
		//封装完毕成功返回
		Resp.Videos = videoList
		Resp.StatusMsg = "success"
		Resp.StatusCode = 200
		return Resp, nil //success
	} else {
		return Resp, nil
	}
}

// 发布评论 TODO 自己只能删除自己的评论，查询出来的时候，通过rabbitmq优化，要求测试结果
func CommentActionFunc(ctx context.Context, req *example.CommentActionReq) (example.CommentActionResp, error) {
	Resp := example.CommentActionResp{}
	user_id := getRedisUserId(req.Token)
	//根据userid查询user
	user := example.User{}
	// TODO 改用从缓存中实现

	db.DB.Table("user").Where("user_id = ?", user_id).Find(&user)
	//判断入参
	//评论主键
	Comment_id := 0
	if req.ActionType == 1 {
		//发布评论,comment_text插入数据
		Count := Comment{
			Content:    req.CommentText,
			CreateDate: newTime(),
			CommentId:  0,
			VideoId:    int32(req.VideoId),
			UserId:     int32(user_id),
			DeletedAt:  "",
		}
		result := db.DB.Table("comments").Create(&Count)
		Comment_id = int(Count.CommentId)
		if result.RowsAffected == 0 {
			zap.S().Error(result.Error)
			Resp.StatusMsg = "somethings error"
			Resp.StatusCode = 500
			return Resp, result.Error
		} else {
			//此处返回只需要id，id作为删除评论，查询评论的条件
			Resp.StatusMsg = "success"
			Resp.StatusCode = 200
			comment := example.Comment{
				Id:         int64(Comment_id),
				User:       &user,
				Content:    "",
				CreateDate: "",
			}
			Resp.Comment = &comment
			return Resp, nil
		}
	} else {
		//删除评论,comment_id,其实就是更新一个deleted_time,
		result := db.DB.Table("comments").
			Model(&Comment{}).
			Where("comment_id = ?", req.CommentId).
			Update("deleted_at", newTime())
		if result.RowsAffected == 0 {
			zap.S().Error(result.Error)
			Resp.StatusMsg = "somethings error"
			Resp.StatusCode = 500
			return Resp, result.Error
		} else {
			Resp.StatusMsg = "success to delete"
			Resp.StatusCode = 200
			Resp.Comment = nil
			return Resp, nil
		}
	}
}

// 查询评论列表,其实就是查询deleted_at是非空的 TODO 查所有评论得按照点赞量倒叙排列,后续更改   点赞量的要求
func QueryCommentListFunc(ctx context.Context, req *example.QueryCommentListReq) (example.QueryCommentListResp, error) {
	Resp := example.QueryCommentListResp{}
	var comments []*example.Comment
	result := db.DB.Table("comments").Where("deleted_at is not null ").Find(&comments)
	// TODO 尚未查询出user复杂sql
	if result.RowsAffected == 0 {
		zap.S().Error(result.Error)
		Resp.StatusMsg = "somethings error"
		Resp.StatusCode = 500
		return Resp, result.Error
	} else {
		Resp.StatusMsg = "success to update"
		Resp.StatusCode = 200
		Resp.Comments = comments
		return Resp, nil
	}
}

// 用户关注操作接口
func LoveActionFunc(ctx context.Context, req *example.LoveActionReq) (example.LoveActionResp, error) {
	Resp := example.LoveActionResp{}
	Idstring, _ := db.RS.Get(req.Token).Result()
	user_id, _ := strconv.Atoi(Idstring)
	log.Println("传递进来的user_id")
	log.Println("从redisuserid")
	log.Println(user_id)
	attention := Attention{
		UserID:   int64(user_id),
		ToUserId: req.ToUserId,
	}
	//先判断参数
	if req.ActionType == 1 {
		//关注 TODO 关注数目的缓存问题
		result := db.DB.Table("attention").Create(&attention)
		if result.RowsAffected == 0 {
			Resp.StatusMsg = "err in server"
			Resp.StatusCode = 500
			return Resp, result.Error
		} else {
			Resp.StatusMsg = "success"
			Resp.StatusCode = 200
			return Resp, result.Error
		}
	} else {
		//取消关注,其实就是删除记录 TODO 删除记录 记得变换缓存
		result := db.DB.Table("attention").Where("user_id = ? and to_user_id = ?", user_id, req.ToUserId).Delete(&attention)
		if result.RowsAffected == 0 {
			Resp.StatusMsg = "err in server"
			Resp.StatusCode = 500
			return Resp, result.Error
		} else {
			Resp.StatusMsg = "success"
			Resp.StatusCode = 200
			return Resp, result.Error
		}
	}
}

// 查询用户关注列表接口
func QueryLoveListFunc(ctx context.Context, req *example.QueryLoveListReq) (example.QueryLoveListResp, error) {
	Resp := example.QueryLoveListResp{}
	//查询一个userlist
	var userList []*example.User
	//TODO sql问题
	SecondQuery := db.DB.Table("attention").Select("to_user_id").Where("user_id = ?", req.UserId)
	result := db.DB.Table("user").Where("user_id in (?)", SecondQuery).Find(&userList)
	Resp.Users = userList
	if result.RowsAffected == 0 {
		Resp.StatusMsg = "error"
		Resp.StatusCode = 500
	} else {
		Resp.StatusMsg = "success"
		Resp.StatusCode = 200
	}
	return Resp, nil
}

// 7查询用户粉丝列表的接口
func QueryFansListFunc(ctx context.Context, req *example.QueryFansListReq) (example.QueryFansListResp, error) {
	Resp := example.QueryFansListResp{}
	//查询一个userlist
	var userList []*example.User
	SecondQuery := db.DB.Table("attention").Select("user_id").Where("to_user_id = ?", req.UserId)
	result := db.DB.Table("user").Where("user_id in (?)", SecondQuery).Find(&userList)
	//result := db.DB.Table("attention").Where("to_user_id = ? ", req.UserId).Find(&userList)
	Resp.Fans = userList
	if result.RowsAffected == 0 {
		Resp.StatusMsg = "error"
		Resp.StatusCode = 500
	} else {
		Resp.StatusMsg = "success"
		Resp.StatusCode = 200
	}
	return Resp, nil
}

// 8查询用户好友列表的接口
func QueryFriendListFunc(ctx context.Context, req *example.QueryFriendListReq) (example.QueryFriendListResp, error) {
	Resp := example.QueryFriendListResp{}
	var userList []*example.User
	result := db.DB.Table("user").
		Select("user.*").
		Joins("JOIN attention AS a1 ON user.user_id = a1.user_id AND a1.to_user_id = ? "+
			"JOIN attention AS a2 ON user.user_id = a2.to_user_id AND a2.user_id = ?", req.UserId, req.UserId).
		Find(&userList)
	Resp.Fans = userList
	if result.RowsAffected == 0 {
		Resp.StatusMsg = "error"
		Resp.StatusCode = 500
	} else {
		Resp.StatusMsg = "success"
		Resp.StatusCode = 200
	}
	return Resp, nil
}
