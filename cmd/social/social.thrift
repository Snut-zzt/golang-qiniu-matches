namespace go social.example
//基本类型
struct Video {
    1:i64 video_id;
    2:User author;
    3:string play_url;
    4:string cover_url;
    5:string title;
    6:i64 favorite_count;
    7:i64 comment_count;
    8:bool is_favorite;
}

struct User {
    1:i64 user_id;
    2:string  user_name;
    3:i64 follow_count;
    4:i64 follower_count;
    5:bool is_follow;
    6:string avatar;
    7:string background_image;
    8:string  signature;
    9:i64 total_favorited;
    10:i64 work_count;
    11:i64 favorite_count;
}
struct Comment {
   1:i64 id; // 视频评论id
   2:User user; // 评论用户信息
   3:string content; // 评论内容
   4:string create_date; // 评论发布日期，格式 毫秒级别的时间戳
}
//1赞操作
struct FavoriteActionReq{
        1:string token;
        2:i64  video_id;
        3:i32  action_type; //1-点赞，2-取消点赞
}
struct FavoriteActionResp{
        1:i32  status_code;
        3:string  status_msg; //返回状态描述
}
//2喜欢列表
struct FavoriteListReq{
        1:i64  user_id;
        2:string   token;
}
struct FavoriteListResp{
        1:i32   status_code;
        2:string  status_msg; //返回状态描述
        3:list<Video> videos;
}
//3发布,删除视频评论
struct CommentActionReq{
        1:string token;
        2:i64  video_id;
        3:i32 action_type; // 1-发布评论，2-删除评论
        4:string comment_text; // 用户填写的评论内容，在action_type=1的时候使用
        5:i64 comment_id; // 要删除的评论id，在action_type=2的时候使用
}
struct CommentActionResp{
        1:i32   status_code;
        2:string  status_msg; //返回状态描述
        3:Comment comment;
}
//4查询视频的列表
struct QueryCommentListReq{
        1:string token;
        2:i64  video_id;
}
struct QueryCommentListResp{
        1:i32   status_code;
        2:string  status_msg; //返回状态描述
        3:list<Comment> comments;
}
//5关注操作
struct LoveActionReq{
        1:string token;
        2:i64  to_user_id;
        3:i32 action_type; //关注或者取消关注
}
struct LoveActionResp{
        1:i32   status_code;
        2:string  status_msg; //返回状态描述
}
//6查询用户关注列表
struct QueryLoveListReq{
        1:i64  user_id;
        2:string   token;
}
struct QueryLoveListResp{
        1:i32   status_code;
        2:string  status_msg; //返回状态描述
        3:list<User> users;
}
//7查询用户粉丝列表的接口
struct QueryFansListReq{
        1:i64  user_id;
        2:string   token;
}
struct QueryFansListResp{
        1:i32   status_code;
        2:string  status_msg; //返回状态描述
        3:list<User> fans;
}
//8，查询好友的接口
struct QueryFriendListReq{
        1:i64  user_id;
        2:string   token;
}
struct QueryFriendListResp{
        1:i32   status_code;
        2:string  status_msg; //返回状态描述
        3:list<User> fans;
}
service SocialService {
    FavoriteActionResp FavoriteAction(1:FavoriteActionReq req); //点赞操作
    FavoriteListResp FavoriteList(1:FavoriteListReq req);  //查询喜欢视频列表
    CommentActionResp CommentAction(1:CommentActionReq req);  //发布评论
    QueryCommentListResp QueryCommentList(1:QueryCommentListReq req); //查询评论列表
    LoveActionResp LoveAction(1:LoveActionReq req); //用户关注操作接口
    QueryLoveListResp QueryLoveList(1:QueryLoveListReq req);  //查询用户关注列表接口
    QueryFansListResp QueryFansList(1:QueryFansListReq req);  //7查询用户粉丝列表的接口
    QueryFriendListResp QueryFriendList(1:QueryFriendListReq req);  //8查询用户好友列表的接口
}